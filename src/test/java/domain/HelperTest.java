package domain;

import org.junit.Test;
import java.util.Optional;

import static org.junit.Assert.*;

public class HelperTest {

    @Test
    public void validateWordTest() {
        assertTrue(Helper.validateWord("pen-pineapple-apple-pen"));
        assertTrue(Helper.validateWord("abA--Ca\"b"));

        assertFalse(Helper.validateWord("siebenhundertsiebenundsiebzigtausendsiebenhundertsiebenundsiebzig"));
        assertFalse(Helper.validateWord("w8"));
    }

    @Test
    public void validateAndFormatNumberTest() {
        assertEquals(Optional.of("2128506"), Helper.validateAndFormatNumber("2-12-85/06"));
        assertEquals(Optional.empty(), Helper.validateAndFormatNumber("3.1415"));
    }
}