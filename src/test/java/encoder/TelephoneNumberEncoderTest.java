package encoder;

import domain.Dictionary;
import domain.DictionaryFactory;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class TelephoneNumberEncoderTest {

    private final Set<String> words = new HashSet<String>(){{
        add("je"); add("da"); add("Tor");
        add("fort"); add("torf"); add("neu");
        add("Bo\""); add("o\"d"); add("bo\"s");
    }};
    private final Dictionary dictionary = DictionaryFactory.getDictFromSet(words);

    @Test
    public void encodeNumberByWords() {
        TelephoneNumberEncoder encoder = new TelephoneNumberEncoder(dictionary);
        List<List<String>> res = Arrays.asList(Collections.singletonList("torf"),
                Collections.singletonList("fort"), Arrays.asList("Tor", "4"));

        assertEquals(new HashSet<>(res), new HashSet<>(encoder.encodeNumberByWords("4824")));

        List<List<String>> res1 = Arrays.asList(Arrays.asList("neu", "o\"d", "5"),
                Arrays.asList("je", "bo\"s", "5"), Arrays.asList("je", "Bo\"", "da"));

        assertEquals(new HashSet<>(res1), new HashSet<>(encoder.encodeNumberByWords("10/783--5")));
    }
}