package encoder;

import org.junit.Test;

import static org.junit.Assert.*;

public class WordEncoderTest {
    @Test
    public void encodeWord() throws Exception {
        assertEquals("95223", WordEncoder.encodeWord("Har\"ry"));
        assertEquals("482", WordEncoder.encodeWord("TO-R"));
    }

}