package domain;

import encoder.WordEncoder;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Set;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;;

import static domain.Helper.validateWord;

public class DictionaryFactory {

    private static final Logger log = Logger.getLogger(DictionaryFactory.class);

    public static Dictionary getDictFromFile(File file) {
        try {
            Set<String> words = getWordsFromFile(file);
            return getDictFromSet(words);
        } catch (FileNotFoundException e) {
            log.error(String.format("File path is incorrect: %s", file));
            return null;
        }
    }

    public static Dictionary getDictFromSet(Set<String> words) {
        Map<String, String> originalToEncoded = new HashMap<>();
        Map<String, List<String>> encodedToOriginal = new HashMap<>();

        words.stream().forEach(word -> {
            String encodedWord = WordEncoder.encodeWord(word);

            List<String> originalWords = encodedToOriginal.getOrDefault(encodedWord, new ArrayList<>());
            originalWords.add(word);
            encodedToOriginal.put(encodedWord, originalWords);

            originalToEncoded.put(word, encodedWord);
        });

        return new domain.Dictionary(encodedToOriginal, originalToEncoded);
    }

    private static Set<String> getWordsFromFile(File file) throws FileNotFoundException {
        Set<String> result = new HashSet<>();
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNext()) {
                String word = scanner.nextLine();
                if (validateWord(word)) {
                    result.add(word);
                } else {
                    log.warn(String.format("Incorrect word at dictionary: '%s'", word));
                }
            }
        }
        return result;
    }
}
