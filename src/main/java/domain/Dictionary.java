package domain;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
* Storing mapping of the original words to encoded as a numbers and backward.
**/
public class Dictionary {

    private final Map<String, List<String>> encodedToOriginal;

    private final Map<String, String> originalToEncoded;

    Dictionary(Map<String, List<String>> encodedToOriginal, Map<String, String> originalToEncoded) {
        this.encodedToOriginal = encodedToOriginal;
        this.originalToEncoded = originalToEncoded;
    }

    public Optional<List<String>> decodeWord(String encodedWord) {
        return Optional.ofNullable(encodedToOriginal.get(encodedWord));
    }

    public Optional<String> encodeWord(String w) {
        return Optional.ofNullable(originalToEncoded.get(w));
    }

    public boolean containsEncoded(String word) {
        return encodedToOriginal.containsKey(word);
    }

    public boolean containsDecoded(String word) {
        return originalToEncoded.containsKey(word);
    }
}
