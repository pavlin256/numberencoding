package domain;

import org.apache.log4j.Logger;
import java.util.Optional;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.String.format;

public class Helper {

    private static final Logger log = Logger.getLogger(Helper.class);

    private static final Pattern wordPattern = Pattern.compile("[a-zA-Z\"\\-]+");
    private static final Pattern numberPattern = Pattern.compile("[0-9/\\-]+");

    public static void writeResults(List<List<String>> results, String num) {
        results.stream().forEach(res ->
           System.out.println(res.stream().reduce(format("%s:", num), (a,b) -> format("%s %s", a, b)))
        );
     }

    public static Optional<String> validateAndFormatNumber(String num) {
        if (num.length() > 50) return Optional.empty();

        Matcher m = numberPattern.matcher(num);
        if (!m.matches()) {
            log.warn(format("Incorrect number received: %s", num));
            return Optional.empty();
        }
        return Optional.of(num.replaceAll("[/\\-]", ""));
    }

    static boolean validateWord(String word) {
        if (word.length() > 50) return false;
        Matcher m = wordPattern.matcher(word);
        return m.matches();
    }
}
