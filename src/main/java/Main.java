import domain.DictionaryFactory;
import encoder.TelephoneNumberEncoder;
import org.apache.log4j.Logger;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Scanner;
import static domain.Helper.*;

public class Main {

    private static Logger log = Logger.getLogger(Main.class);

    /*
    * @param args[0] - path to dictionary file
    * @param args[1] - path to input file (with telephone numbers)
    * */
    public static void main(String[] args) {
        try {
            File dictFile = new File(args[0]);
            File numFile = new File(args[1]);

            domain.Dictionary dictionary = DictionaryFactory.getDictFromFile(dictFile);

            if (dictionary != null) {
                log.debug("Dictionary created successfully");
                TelephoneNumberEncoder encoder = new TelephoneNumberEncoder(dictionary);
                try (Scanner scan = new Scanner(numFile)) {
                    while (scan.hasNext()) {
                        String num = scan.nextLine();
                        log.debug(String.format("Start encoding number '%s'", num));
                        List<List<String>> results = encoder.encodeNumberByWords(num);
                        log.debug(String.format("Number '%s' successfully encoded to '%s'", num, results));
                        writeResults(results, num);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            log.error("Incorrect file path", e);
        } catch (Exception e) {
            log.error("Unexpected exception occurs: ", e);
        }
    }
}
