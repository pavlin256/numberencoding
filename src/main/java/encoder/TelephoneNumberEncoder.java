package encoder;

import java.util.*;
import domain.Dictionary;
import static domain.Helper.*;

public class TelephoneNumberEncoder {

    private final Dictionary dictionary;

    /**
     * Storage for encoding results
     **/
    private List<List<String>> result;

    /**
     * Contains positions of {@link #numberToEncode} that was used as a beginnings
     * of encoded words.
     **/
    private Set<Integer> usedPositions;

    /**
     * String which is represents the number that will be encoded
     * **/
    private String numberToEncode;

    public TelephoneNumberEncoder(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    /**
    * @param num number to be encoded
    * @return the List of all encodings number by words
    **/
    public List<List<String>> encodeNumberByWords(String num) {
        validateAndFormatNumber(num).ifPresent(a -> {
            result = new ArrayList<>();
            usedPositions = new HashSet<>();
            numberToEncode = a;
            encode(0, 0, new ArrayList<>(), true);
        });
        return result;
    }

    /**
     * Generate all possible number encodings. Store results at {@link #result}.
     *
     * @param start position at the {@link #numberToEncode} represents the beginning of the word
     * that is the next candidate for the encoding
     * @param current current position af the {@link #numberToEncode}
     * @param acc contains words that are encode prefix of the {@link #numberToEncode} till {@code start}
     * @param isLastDigitEncoded is false when digit {@code current - 1} was encoded as a digit
     * **/
    private void encode(int start, int current, List<String> acc, boolean isLastDigitEncoded) {
        if (current == numberToEncode.length()) {
            if (current == start) {
                result.add(acc);
                updateUsedPositions(acc);
            }
        } else {
            if (dictionary.containsEncoded(numberToEncode.substring(start, current + 1))) {
                String word = numberToEncode.substring(start, current + 1);
                dictionary.decodeWord(word)
                    .orElseThrow(()->new IllegalStateException(String.format("Word %s not found in dict",word))) //should not happen
                    .forEach(w -> encode(current+1, current+1, updateAcc(acc, w), true));
            }

            encode(start, current + 1, acc, true);

            if (current == start && isLastDigitEncoded && !usedPositions.contains(current)) {
                String digit = String.valueOf(numberToEncode.charAt(current));
                encode(current + 1, current + 1, updateAcc(acc, digit), false);
            }
        }
    }

    private void updateUsedPositions(List<String> acc) {
        int pos = 0;
        for (String w: acc) {
            if (dictionary.containsDecoded(w)) usedPositions.add(pos);
            pos = pos + dictionary.encodeWord(w).orElse(w).length();
        }
    }

    private List<String> updateAcc(List<String> oldAcc, String newEntry) {
        List<String> newAcc = new ArrayList<>(oldAcc);
        newAcc.add(newEntry);
        return newAcc;
    }
}
