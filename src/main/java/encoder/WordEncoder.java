package encoder;

import java.util.HashMap;
import java.util.Map;

public class WordEncoder {
    private static final Map<Character, Character> characterToDigit = new HashMap<Character, Character>()
    {{
        put('e', '0');
        put('j', '1'); put('n', '1'); put('q', '1');
        put('r', '2'); put('w', '2'); put('x', '2');
        put('d', '3'); put('s', '3'); put('y', '3');
        put('f', '4'); put('t', '4');
        put('a', '5'); put('m', '5');
        put('c', '6'); put('i', '6'); put('v', '6');
        put('b', '7'); put('k', '7'); put('u', '7');
        put('l', '8'); put('o', '8'); put('p', '8');
        put('g', '9'); put('h', '9'); put('z', '9');
    }};

    public static String encodeWord(String word) {
        return word.toLowerCase().chars()
                .mapToObj(i -> encodeChar((char)i))
                .filter(i -> i !='\u0000')
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    private static char encodeChar(char x) {
        return characterToDigit.getOrDefault(x, '\u0000');
   }
}
